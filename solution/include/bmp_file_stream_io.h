#include "io_result_enums.h"
#include "struct/image.h"
#include  <stdint.h>
#include <stdio.h>

enum read_status read_from_bmp_stream( FILE* in, struct image* img );

enum write_status write_to_bmp_stream( FILE* out, const struct image* img );
