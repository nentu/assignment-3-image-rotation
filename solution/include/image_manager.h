#include "./struct/image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

 #ifndef IMAGEMANAGER_H
 #define IMAGEMANAGER_H
 
#include <stdint.h>

void image_print(char* label, struct image const img);

void image_set(struct image* img, size_t x, size_t y, struct pixel new_value );

struct pixel image_get(const struct image* img, size_t x, size_t y);

void image_fill(struct image* img, uint64_t width, uint64_t height);

void image_free(struct image* image);

struct image image_copy(struct image const source);
#endif
