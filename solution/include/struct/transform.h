#ifndef TRANSFORMSTRUCTURE_H
#define TRANSFORMSTRUCTURE_H

#include "struct/image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef size_t (*coord_transform) (struct image const new_image, size_t source_x, size_t source_y);

// typedef size_t (coord_transform)(struct image const new, size_t source_x, size_t source_y);

struct transform_rule{
  coord_transform get_x;
  coord_transform get_y;
  bool is_rotated;
};

enum transform_type{
    DEFAULT = 0,
    RIGHT_90,
    TURN_180,
    LEFT_90,
    TRANSPONCE_SIDE_DIAGONAL,
    X_REFLECT
};

#endif
