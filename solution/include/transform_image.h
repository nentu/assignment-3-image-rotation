#include "image_manager.h"
#include "struct/transform.h"

void apply_transorm(struct image* rotated, struct image const source, enum transform_type t_type );

struct image rotate(struct image const source, uint64_t angle );
