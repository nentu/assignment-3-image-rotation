#include "struct/transform.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


struct transform_rule get_rule(enum transform_type type);
