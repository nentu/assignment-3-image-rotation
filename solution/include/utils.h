#include "../include/image_manager.h"
#include "../include/struct/bmpheader.h"
#include  <stdint.h>
#include <stdio.h>

struct bmp_header header_from_image(const struct image* source);

bool header_is_correct(const struct bmp_header *header);

long get_padding(const struct image* image);
