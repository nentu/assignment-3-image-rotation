#include "../include/image_manager.h"
#include "../include/io_result_enums.h"
#include "../include/struct/bmpheader.h"
#include "../include/utils.h"
#include  <stdint.h>
#include <stdio.h>

#define FREAD(link, size, count, stream) fread(link, size, count, stream); \
        if (ferror(stream)){return READ_INVALID_BITS;}

#define FWRITE(link, size, count, stream) fwrite(link, size, count, stream); \
        if (ferror(stream)){return WRITE_ERROR;}

#define FSEEK_R(stream, offset, start_type) fseek(stream, offset, start_type); \
        if (ferror(stream)){return READ_INVALID_BITS;}

#define FSEEK_W(stream, offset, start_type) fseek(stream, offset, start_type); \
        if (ferror(stream)){return WRITE_ERROR;}




enum read_status read_from_bmp_stream( FILE* in, struct image* img ){
    struct bmp_header header;

    if (!fread(&header, sizeof(struct bmp_header), 1, in) || !header_is_correct(&header))  //read header
      return READ_INVALID_HEADER;

    FSEEK_R(in, header.bOffBits, SEEK_SET);

    image_fill(img, header.biWidth, header.biHeight);

    for (size_t y = 0; y < img->height; y++)
    {
        for (size_t x = 0; x < img->width; x++)
        {
            struct pixel px;
            FREAD(&px, sizeof(struct pixel), 1, in)
            image_set(img, x, (img->height - 1) - y, px);
        }
        FSEEK_R(in, (long) get_padding(img), SEEK_CUR); 
    }

    return READ_OK;
}



enum write_status write_to_bmp_stream( FILE* out, const struct image* image ){
  struct bmp_header header = header_from_image(image);

  FWRITE(&header, sizeof(struct bmp_header), 1, out);

  FSEEK_W(out, header.bOffBits, SEEK_SET);

  for (size_t y = 0; y < image->height; y++)
    {
        for (size_t x = 0; x < image->width; x++)
        {
            struct pixel px = image_get(image, x, (image->height - 1) - y);
            FWRITE(&px, sizeof(struct pixel), 1, out);
        }
        FSEEK_W(out, get_padding(image), SEEK_CUR); 
    }

  return WRITE_OK;
}
