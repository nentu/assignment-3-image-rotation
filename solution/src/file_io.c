#include "../include/bmp_file_stream_io.h"
#include "../include/io_result_enums.h"
#include "../include/struct/image.h"
#include <stdio.h>

enum read_status read_image(struct image* image, const char* file_name){
    FILE* f = fopen(file_name, "rb");
    if (f == NULL)
        return READ_INVALID_PATH;
    enum read_status res =  read_from_bmp_stream(f, image);
    fclose(f);
    return res;
}

enum write_status write_image(const struct image* image, const char* file_name){
    FILE* f = fopen(file_name, "wb");
    if (f == NULL)
        return WRITE_INVALID_PATH;
    enum write_status res =  write_to_bmp_stream(f, image);

    uint8_t eof = 0;
    fwrite(&eof, 1, 1, f);
    fclose(f);
    return res;
}
