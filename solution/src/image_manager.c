#include "../include/image_manager.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>



void image_print(char* label, struct image const img){
    printf("##############\n");
    printf("%s", label);
    for (size_t y = 0; y < img.height; y++){
        printf("\n r:\t");
        for (size_t x = 0; x < img.width; x++){
            printf("%" PRId8 "\t", image_get(&img, x, y).r);
        }
        printf("\n g:\t");
        for (size_t x = 0; x < img.width; x++){
            printf("%" PRId8 "\t", image_get(&img, x, y).g);
        }
        printf("\n b:\t");
        for (size_t x = 0; x < img.width; x++){
            printf("%" PRId8 "\t", image_get(&img, x, y).b);
        }
        printf("\n");
    }
    printf("##############\n");
}

void image_set(struct image* img, size_t x, size_t y, struct pixel new_value ){
    img->data[y*img->width + x] = new_value;
}

struct pixel image_get(const struct image* img, size_t x, size_t y){
    return img->data[y*img->width + x];
}

void image_fill(struct image* img, uint64_t width, uint64_t height){
    img->width = width;
    img->height = height;
    img->data = (struct pixel*) malloc(sizeof(struct pixel) * (img->width *img->height));
}

struct image image_copy(struct image const source){
  struct image new_image;
  image_fill(&new_image, source.width, source.height);
  for (size_t y = 0; y < new_image.height; y++){
    for (size_t x = 0; x < new_image.width; x++){
      image_set(&new_image, x, y, image_get(&source, x, y));
    }
  }
  return new_image;
}

void image_free(struct image* image){
    free(image->data);
}
