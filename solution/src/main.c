#include "image_manager.h"
#include "bmp_file_stream_io.h"
#include "file_io.h"
#include "io_result_enums.h"
#include "transform_image.h"
#include "utils.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

char* read_errors[] = {
    "OK",
    "Invalid path",
    "Invalid signature",
    "Invalid bits",
    "Invalid header"
};

char* write_errors[] = {
    "OK",
    "Invalid path",
    "Error during writing the file"
};



int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    bool isTest = false;

    if (argc!=4 && !isTest){
        printf("Invalid count of args: expected 4, inserted: %d\n",  argc);
        return 1;
    }
    char* source_name;
    char* res_name;
    char* angle_arg;
    if (isTest){
        source_name = "/mnt/c/Users/zam12/Videos/PL/assignment-3-image-rotation/solution/res/img.bmp";
        res_name = "/mnt/c/Users/zam12/Videos/PL/assignment-3-image-rotation/solution/res/res.bmp";
        angle_arg = "-90";
    }else{
        source_name = argv[1];//"/mnt/c/Users/zam12/Videos/PL/assignment-3-image-rotation/solution/res/img.bmp";
        res_name = argv[2];//"/mnt/c/Users/zam12/Videos/PL/assignment-3-image-rotation/solution/res/res.bmp";
        angle_arg = argv[3];//"90";
    }


    uint16_t angle = atoi(angle_arg);


    struct image image;

    enum read_status read_status = read_image(&image, source_name);
    if (read_status != READ_OK){
        printf("Read error: %s\n", read_errors[read_status]);
        image_free(&image);
        return 2;
    }
    // image_print("read image", image);


    
    struct image new_image = rotate(image, angle);


    enum write_status write_status = write_image(&new_image, res_name);
    if (write_status != WRITE_OK){
        printf("Write error: %s\n", write_errors[write_status]);
        image_free(&image);
        image_free(&new_image);
        return 3;
    }
    // image_print("write image", new_image);

    image_free(&image);
    image_free(&new_image);
    return 0;
    
}
