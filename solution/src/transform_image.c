#include "../include/struct/image.h"
#include "../include/image_manager.h"
#include "../include/transform_rules.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>


void transform_image(
                      struct image* new_image,
                      struct image const source,
                      const struct transform_rule rule
                    ){
  if (rule.is_rotated)
    image_fill(new_image, source.height, source.width);
  else
    image_fill(new_image, source.width, source.height);
  for (size_t y = 0; y < source.height; y++){
    for (size_t x = 0; x < source.width; x++){
      image_set(
        new_image, 
        rule.get_x(*new_image, x, y), 
        rule.get_y(*new_image, x, y), 
        image_get(&source, x, y));
    }
  }
}


void apply_transorm(struct image* rotated, struct image const source, enum transform_type t_type ){
  struct transform_rule rule = get_rule(t_type);
  transform_image(
    rotated,
    source,
    rule
  );
}


struct image rotate (struct image const source, uint64_t angle ){
  struct image rotated;

  uint64_t count_90_right = (4 + (angle / 90) % 4) % 4;

  apply_transorm(&rotated, source, count_90_right);

  return rotated;
}
