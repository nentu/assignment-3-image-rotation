#include "../include/struct/image.h"
#include "../include/struct/transform.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define NEW_RULE(name, x_rule, y_rule) \
  static size_t name##_get_x(struct image const new, size_t x, size_t y){ \
    return (x_rule) + 0*(new.width+x+y);   \
  } \
  static size_t name##_get_y(struct image const new, size_t x, size_t y){  \
    return (y_rule) + 0*(new.width+x+y); \
  } 


NEW_RULE(default, x, y)

NEW_RULE(right_90, (new.width - 1) - y, x)

NEW_RULE(left_90, y, (new.height - 1) - x)

NEW_RULE(turn_180, (new.width - 1) - x, (new.height - 1) - y)

NEW_RULE(transponce_side_diagonal, y, x)

NEW_RULE(x_reflect, (new.width - 1) - x, y)

struct transform_rule res[] = {
    {
      default_get_x,
      default_get_y,
      false
    },
    {
      right_90_get_x,
      right_90_get_y,
      true
    },
    {
      turn_180_get_x,
      turn_180_get_y,
      false
    },
    {
      left_90_get_x,
      left_90_get_y,
      true
    },
    {
      transponce_side_diagonal_get_x,
      transponce_side_diagonal_get_y,
      true
    },
    {
      x_reflect_get_x,
      x_reflect_get_y,
      false
    }
};

struct transform_rule get_rule(enum transform_type type){
    return res[type];
}
