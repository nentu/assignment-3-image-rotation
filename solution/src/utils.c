#include "../include/image_manager.h"
#include "../include/struct/bmpheader.h"
#include  <stdint.h>
#include <stdio.h>

#define BM_CONST 19778

int get_padding(const struct image* image){
    return (int) (4 - (image->width * sizeof(struct pixel)) % 4) % 4;
}

struct bmp_header header_from_image(const struct image* source){
    uint32_t data_size = source->height * (source->width * sizeof(struct pixel) + get_padding(source));
    struct bmp_header res = {
        .bfType = (uint16_t) BM_CONST,
        .bfileSize = sizeof(struct bmp_header) + data_size,
        .bOffBits = sizeof(struct bmp_header),
        .biSizeImage = data_size,
        
        .biSize = 40,
        .biWidth = source->width,
        .biHeight = source->height,
        .biPlanes = 1,
        .biBitCount = 24
        };

    return res;
}

bool header_is_correct(const struct bmp_header *header) {
    return 
            header->biBitCount == 24 &&
            header->bfType == BM_CONST;

}
